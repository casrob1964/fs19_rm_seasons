@ECHO off

SET filename="FS19_RM_Seasons.zip"

IF EXIST %filename% (
    DEL  %filename% > NUL
)

"7z.exe" a -tzip %filename% ^
   -i!*.lua ^
   -i!*.dds ^
   -i!*.i3d ^
   -i!*.i3d.shapes ^
   -i!*.grle ^
   -i!*.i3d.anim ^
   -i!*.ogg ^
   -i!*.xml ^
   -xr!Substance ^
   -xr!.mayaSwatches ^
   -xr!.idea ^
   -xr!resources/trees/*.i3d ^
   -xr!resources/trees/*.i3d.shapes ^
   -aoa -r ^

IF %ERRORLEVEL% NEQ 0 ( exit 1 )

PAUSE
